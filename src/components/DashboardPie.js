import * as React from 'react'
import { ResponsivePie } from '@nivo/pie'
import { CustomColors } from './common/variables'
import './common/App.css'

class CustomPie extends React.Component {
  render () {
    return (
      <div className='container'>
        <div className='header'>
          {this.props.header}
        </div>
        <div className='visualization'>
          <ResponsivePie
            data={this.props.data}
            margin={{
              'top': 40,
              'right': 80,
              'bottom': 160,
              'left': 80
            }}
            innerRadius={0.5}
            padAngle={0.7}
            cornerRadius={3}
            colors={CustomColors}
            colorBy='id'
            borderColor='inherit:darker(0.6)'
            radialLabelsSkipAngle={10}
            radialLabelsTextXOffset={6}
            radialLabelsTextColor='#333333'
            radialLabelsLinkOffset={0}
            radialLabelsLinkDiagonalLength={16}
            radialLabelsLinkHorizontalLength={24}
            radialLabelsLinkStrokeWidth={1}
            radialLabelsLinkColor='inherit'
            slicesLabelsSkipAngle={10}
            slicesLabelsTextColor='#fff'
            animate
            motionStiffness={90}
            motionDamping={15}
            legends={[
              {
                'anchor': 'bottom',
                'direction': 'column',
                'translateY': 120,
                'itemWidth': 150,
                'itemHeight': 24,
                'symbolSize': 14,
                'symbolShape': 'circle'
              }
            ]}
          />
        </div>
      </div>
    )
  }
}

export const DashboardPie = CustomPie
